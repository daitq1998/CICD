# FROM openjdk:8-jdk-alpine
# # VOLUME /tmp
# ARG JAR_FILE
# COPY /*.jar app.jar
# ENTRYPOINT ["java","-jar","/app.jar"]
FROM openjdk:8-jdk-alpine
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
# VOLUME /tmp
EXPOSE 8080
ARG JAR_FILE=/*.jar
WORKDIR /opt/app
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","app.jar"]
